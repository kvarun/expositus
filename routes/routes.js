// create our router
var express = require('express');
var router = express.Router();
var User   = require('../models/user');

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Inside API.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// on routes that end in /api/users
// ----------------------------------------------------
router.route('/users')

	// create a user
	.post(function(req, res) {
		
		var newUser = new User();		// create a new instance of the User model
		newUser.facebook.id    = res.headers.id;
        newUser.facebook.token = res.headers.token;
        newUser.facebook.name  = res.headers.name;
        newUser.facebook.email = res.headers.email;
        newUser.facebook.friends = res.headers.friends;

		bear.save(function(err) {
			if (err)
				res.send(err);

			res.json({ message: 'user created!' });
		});

		
	})

	// get all users 
	.get(function(req, res) {
		User.find(function(err, users) {
			if (err)
				res.send(err);

			res.json(users);
		});
	});

// on routes that end in /api/users/:user_id
// ----------------------------------------------------
router.route('/users/:user_id')

	// get the user with that id
	.get(function(req, res) {
		User.findById(req.params.user_id, function(err, user) {
			if (err)
				res.send(err);
			res.json(user);
		});
	})

	// update the user with this id
	.put(function(req, res) {
		User.findById(req.params.user_id, function(err, user) {

			if (err)
				res.send(err);

			user.name = req.headers.name;
			user.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'User updated!' });
			});

		});
	})

	// delete the user with this id
	.delete(function(req, res) {
		User.remove({
			_id: req.params.user_id
		}, function(err, user) {
			if (err)
				res.send(err);

			res.json({ message: 'Successfully deleted' });
		});
	});

module.exports = router;