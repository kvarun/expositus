var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/nodedata');

module.exports = mongoose.model('UserFeedback',{
	feedbackId : String,
    toUser : String,
    fromUser : String,
    msgContent: String,
    postedAt : Date,
    flagged : Number
});